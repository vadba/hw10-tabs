const listenerItem = () => {
    const listItem = document.querySelectorAll('.tabs-title');
    const listItemContent = document.querySelectorAll('.list-item');

    listItem.forEach(item => {
        item.addEventListener('click', e => {
            listItem.forEach(item => {
                item.classList.remove('active');
            });

            e.target.classList.toggle('active');

            listItemContent.forEach(item => {
                item.classList.remove('active-tab');
            });
        });
    });

    for (let index = 0; index < listItemContent.length; index++) {
        listItem[index].addEventListener('click', e => {
            listItemContent[index].classList.toggle('active-tab');
        });
    }
};

listenerItem();
